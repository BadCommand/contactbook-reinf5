package contactbook;

class Contact {

	/*
	 * Implement a class Contact which contains a person's name, email address and
	 * phone number. Implement a class ContactBook which contains an array of
	 * contacts and provides the following methods:
	 * 
	 * 
	 * capacity(): int returns the maximum number of contacts
	 * 
	 * size(): int returns the actual number of contacts
	 * 
	 * addContact(contact: Contact): boolean adds a contact if no contact with the
	 * same already exists; the return value indicates if the addition was
	 * successful
	 * 
	 * findContact(name: String): Contact returns the contact with the given name,
	 * or null if no such contact exists
	 * 
	 * removeContact(name: String): boolean removes the contact with the given name;
	 * the return value indicates if the removal was successful
	 * 
	 * The class ContactBook has a constructor that allows to specify the initial
	 * capacity. As contacts are added, the capacity shall be increased if
	 * necessary.
	 * 
	 * 
	 */

	String name;
	String mailAddress;
	String phoneNumb;

	public Contact(String name, String mailAddress, String phoneNumb) {
		this.name = name;
		this.mailAddress = mailAddress;
		this.phoneNumb = phoneNumb;
	}

	public String getName() {

		return name;
	}

	public String getMailAdress() {

		return mailAddress;
	}

	public String getPhoneNumb() {

		return phoneNumb;
	}

}
