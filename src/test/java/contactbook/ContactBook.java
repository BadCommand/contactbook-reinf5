package contactbook;

import java.util.Arrays;

public class ContactBook {

	Contact contacts[];
	int contactsSize = 0;
	int index = -1;
	boolean createIndex;

	public ContactBook(int capacity) {
		contacts = new Contact[capacity];
	}

	public int getCapacity() {

		return contacts.length;
	}

	public int getSize() {

		for (int i = 0; i < contacts.length; i++) {
			if (contacts[i] != null) {
				contactsSize += 1;
			}
		}

		return contactsSize;
	}

	public boolean addContact(Contact newContact) {		
		createIndex = true;

		for (int i = 0; i < contacts.length; i++) {
			if (contacts[i] != null) {
				if (contacts[i].equals(newContact)) {
					return false;
				}
			}
		}

		for (int i = 0; i < contacts.length; i++) {
			if (contacts[i] == null) {
				index = i;
				createIndex = false;
				break;
			}
		}

		if (createIndex == true) {
			contacts = Arrays.copyOf(contacts, contacts.length + 1);
			index = (contacts.length) - 1;
		}

		contacts[index] = newContact;
		return true;
	}

	public Contact findContact(String name) {

		for (int i = 0; i < contacts.length; i++) {
			if (contacts[i] != null) {
				if (contacts[i].name.equals(name)) {
					return contacts[i];
				}
			}
		}
		return null;
	}

	public boolean removeContact(String name) {

		for (int i = 0; i < contacts.length; i++) {
			if (contacts[i] != null) {
				if (contacts[i].name.equals(name)) {
					contacts[i] = null;
					return true;
				}
			}
		}

		return false;
	}
}
